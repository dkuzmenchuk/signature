from __future__ import absolute_import
from __future__ import division
# Placeholder for import for type annotations
from __future__ import print_function

import base64
import binascii

# Special imports
from absl import app
from absl import flags
from absl import logging
import tink
from tink import cleartext_keyset_handle
from tink import signature


FLAGS = flags.FLAGS

flags.DEFINE_string('keyset', None,
                    'Keyset used for the signature operation.')
flags.DEFINE_string('data', None,
                    'Input data.')
flags.DEFINE_string('signature', None,
                    'Signature')

def main(argv):
  del argv  # Unused.

  # Initialise Tink
  try:
    signature.register()
  except tink.TinkError as e:
    logging.exception('Error initialising Tink: %s', e)
    return 1

  # Read the keyset into a keyset_handle
  try:
    logging.info('keyset: %s', FLAGS.keyset)
    keyset_handle = cleartext_keyset_handle.read(tink.JsonKeysetReader(FLAGS.keyset))
  except tink.TinkError as e:
    logging.exception('Error reading key: %s', e)
    return 1

  message = FLAGS.data

  # Get the primitive
  try:
    cipher = keyset_handle.primitive(signature.PublicKeyVerify)
  except tink.TinkError as e:
    logging.exception('Error creating primitive: %s', e)
    return 1

  # Verify message
  try:
    base64url_signature = FLAGS.signature

    decoded_signature = base64.urlsafe_b64decode(base64url_signature)
  except binascii.Error as e:
    logging.exception('Error reading expected code: %s', e)
    return 1

  try:
    logging.info('message: %s', message)
    cipher.verify(decoded_signature, message.encode())
    logging.info('Signature verification succeeded.')
    return 0
  except binascii.Error as e:
    logging.exception('Error reading expected signature: %s', e)
  except tink.TinkError as e:
    logging.info('Signature verification failed. %s', e)
    return 1


if __name__ == '__main__':
  flags.mark_flags_as_required([
      'keyset', 'data', 'signature'])
  app.run(main)
