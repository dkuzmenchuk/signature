# Python digital signature verification

Verify a signature:

```shell
$ python main.py \
    --keyset "<keyset_json>" \
    --data "<data_string>" \
    --signature "<base64_urlencoded_signature>"
```